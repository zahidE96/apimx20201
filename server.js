var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

/*var db= null, collection = null;
var mongodb = require('mongodb');
var Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
      ObjectID = require('mongodb').ObjectID,
      Binary = require('mongodb').Binary,
      GridStore = require('mongodb').GridStore,
      Grid = require('mongodb').Grid,
      Code = require('mongodb').Code,
      assert = require('assert');
      MongoClient.connect('mongodb://admin123:admin123@ds163757.mlab.com:63757/zramirez', (err,client) => {
    try {
      if(err)
      {
        return console.log("No conecto" + err);
      }
      else {
          console.log("CONEXION BD: OK");
          db = client.db('fgutierrez');
    }
   } catch(err) {
           console.log(err.message)
   }
 });*/

var requestjson= require('request-json');
var urlRaizMLab= 'https://api.mlab.com/api/1/databases/zramirez/collections/';
var apiKey= 'apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
var clienteMLabRaiz;

var urlClientes= 'https://api.mlab.com/api/1/databases/zramirez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt'
var movimientosJSON=require('./movimientosv2.json');
var bodyParser= require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req, res){
  //res.send("HOLA MUNDO");
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.get('/Clientes/:idcliente',function(req, res){
  //res.send("HOLA MUNDO");
  res.send('CLiente no: '+req.params.idcliente);
})

app.post('/',function(req, res){
  res.send("Se ha recibido la petición post.");
})

app.put('/',function(req, res){
  res.send("Se ha recibido la petición put ->.");
})

app.delete('/',function(req, res){
  res.send("Se ha recibido la petición delete.");
})

app.get('/v1/movimientos', function(req, res){
  res.sendfile('movimientosv1.json');
})

app.get('/v2/movimientos/:indice', function(req, res){
  console.log(req.params.indice);
  res.send(movimientosJSON[req.params.indice]);
})

app.get('/v3/movimientosquery', function(req, res){
  console.log(req.query);
  res.send('Recibido');
})

app.post('/v3/movimientos', function(req, res){
  var nuevo= req.body;
  nuevo.id=movimientosJSON.length+1;
  movimientosJSON.push(nuevo);
  res.send('Movimiento dado de alta');
})

app.get('/v2/movimientos', function(req, res){
  res.json(movimientosJSON);
})

app.get('/Clientes', function(req, res){
  var clienteMLab = requestjson.createClient(urlClientes);
  clienteMLab.get('', function(err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

app.post('/login', function(req, res){
  res.set("Access-Control-Allow-Headers", "Content-Type");
  var email= req.body.email;
  var password= req.body.password;
  var query= 'q={"email":"'+email+'","password":"'+password+'"}';
  clienteMLabRaiz= requestjson.createClient(urlRaizMLab+"/Usuarios?"+apiKey+"&"+query);
  console.log(urlRaizMLab+"/Usuarios?"+apiKey+"&"+query);
  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length==1){//login ok
        res.status(200).send('Usuario logeado');

      }else{
        res.status(404).send('Usuario no encontrado, regístrese');
      }
    }
  })
})
//insertar un cliente nuevo


app.post('/Registrar', function(req, res){
  res.set("Access-Control-Allow-Headers", "Content-Type");
  var email= req.body.email;
  var password= req.body.password;
  var json= '{"email":"'+email+'","password":"'+password+'"}';
  var clienteMLab = requestjson.createClient(urlRaizMLab+"/Usuarios?"+apiKey);
  clienteMLab.get('',function(err,resM,body){
    var data={
      email:email,
      password:password
    };
  clienteMLab.post('',data,function(err,resM,body){
    if (!err) {
      res.status(200).send('Usua registrado');
    } else {
      res.status(200).send('Usua no registrado');
    }
  })
  })
})

//insertar un producto nuevo


app.post('/RegistrarProd', function(req, res){
  res.set("Access-Control-Allow-Headers", "Content-Type");
  var producto= req.body.producto;
  var tipo_producto= req.body.tipo_producto;
  var empresa= req.body.empresa;
  var promocion= req.body.promocion;
  var producto_banc= req.body.producto_banc;
  var tienda= req.body.tienda;

  //var json= '{"email":"'+email+'","password":"'+password+'"}';
  var clienteMLab = requestjson.createClient(urlRaizMLab+"/Productos?"+apiKey);
  clienteMLab.get('',function(err,resM,body){
    var data={
      producto:producto,
      tipo_producto:tipo_producto,
      empresa:empresa,
      promocion:promocion,
      producto_banc:producto_banc,
      tienda:tienda
    };
  clienteMLab.post('',data,function(err,resM,body){
    if (!err) {
      res.status(200).send('Usua registrado');
    } else {
      res.status(200).send('Usua no registrado');
    }
  })
  })
})
